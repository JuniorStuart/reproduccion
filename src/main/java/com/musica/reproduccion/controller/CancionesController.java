package com.musica.reproduccion.controller;

import com.musica.reproduccion.entidades.Canciones;
import com.musica.reproduccion.service.CancionesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api/v1/Canciones")
public class CancionesController {


    @GetMapping()
    public List <Canciones> getAllCanciones(){
        return cancionesService.getcanciones();
    }


    @Autowired
    private CancionesService cancionesService;
    @PostMapping
    public void saveUpdate(@RequestBody Canciones canciones){
        cancionesService.saveOrUpdate(canciones);
    }

    @DeleteMapping("/{id}")
    public void saveUpdate(@PathVariable("id")Long id){
        cancionesService.delete(id);
    }



}
