package com.musica.reproduccion.controller;

import com.musica.reproduccion.entidades.Canciones;
import com.musica.reproduccion.entidades.ListaCancion;
import com.musica.reproduccion.entidades.Listas;
import com.musica.reproduccion.service.ListasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api/v1/Listas")
public class ListasController {
    @Autowired
    private ListasService listasService;

    @GetMapping()
    public List<Listas> getAllListas(){
        return listasService.getListas();
    }
    @PostMapping
    public void saveUpdate(@RequestBody Listas listas){
        listasService.saveOrUpdate(listas);
    }

    @DeleteMapping("/{id}")
    public void saveUpdate(@PathVariable("id")Long id){
        listasService.delete(id);
    }

}
