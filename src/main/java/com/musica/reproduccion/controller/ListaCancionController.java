package com.musica.reproduccion.controller;

import com.musica.reproduccion.entidades.ListaCancion;
import com.musica.reproduccion.service.ListaCancionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/ListaCancion")
public class ListaCancionController {



    @Autowired
    private ListaCancionService listaCancionService;

    @GetMapping()
    public List<ListaCancion> getAllListaCancionController(){
        return listaCancionService.getListaCancion();
    }


    @PostMapping
    public void saveUpdate(@RequestBody ListaCancion listaCancion){
        listaCancionService.saveOrUpdate(listaCancion);
    }

    @DeleteMapping("/{id}")
    public void saveUpdate(@PathVariable("id")Long id){
        listaCancionService.delete(id);
    }
}
