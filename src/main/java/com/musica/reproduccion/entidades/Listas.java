package com.musica.reproduccion.entidades;

import jakarta.persistence.*;
import lombok.Data;


@Data
@Entity
@Table(name = "tbl_listas")
public class    Listas {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long  numeroLista;
    private String descripcion;


}
