package com.musica.reproduccion.entidades;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "lista_canciones")
public class ListaCancion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "lista_id")
    private Listas lista;

    @ManyToOne
    @JoinColumn(name = "cancion_id")
    private Canciones cancion;
}

