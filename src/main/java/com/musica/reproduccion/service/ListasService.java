package com.musica.reproduccion.service;

import com.musica.reproduccion.entidades.Listas;
import com.musica.reproduccion.repository.ListasRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ListasService {
    @Autowired
    ListasRepository listasRepository;

    public List<Listas> getListas(){
        return  listasRepository.findAll();
    }

    public void saveOrUpdate(Listas listas){
        listasRepository.save(listas);
    }
    public void delete(Long id){
        listasRepository.deleteById(id);
    }

}
