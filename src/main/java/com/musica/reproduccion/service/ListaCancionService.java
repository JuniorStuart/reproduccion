package com.musica.reproduccion.service;

import com.musica.reproduccion.entidades.Canciones;
import com.musica.reproduccion.entidades.ListaCancion;
import com.musica.reproduccion.repository.ListaCancionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ListaCancionService {
    @Autowired
    ListaCancionRepository listaCancionRepository;
    public List<ListaCancion>getListaCancion(){
        return listaCancionRepository.findAll();
    }
    public void saveOrUpdate(ListaCancion id){
        listaCancionRepository.save(id);
    }
    public void delete(Long id){
        listaCancionRepository.deleteById(id);
    }


}
