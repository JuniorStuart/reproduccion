package com.musica.reproduccion.service;

import com.musica.reproduccion.entidades.Canciones;
import com.musica.reproduccion.entidades.Listas;
import com.musica.reproduccion.repository.CancionesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CancionesService {
    @Autowired
    CancionesRepository cancionesRepository;

    public List<Canciones> getcanciones(){
        return cancionesRepository.findAll();
    }
    public void saveOrUpdate(Canciones id){
        cancionesRepository.save(id);
    }
    public void delete(Long id){
        cancionesRepository.deleteById(id);
    }
}
