package com.musica.reproduccion.repository;


import com.musica.reproduccion.entidades.Canciones;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CancionesRepository extends JpaRepository<Canciones, Long> {


}
