package com.musica.reproduccion.repository;

import com.musica.reproduccion.entidades.ListaCancion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ListaCancionRepository extends JpaRepository <ListaCancion,Long> {
}
