package com.musica.reproduccion.repository;

import com.musica.reproduccion.entidades.Listas;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ListasRepository extends JpaRepository<Listas,Long> {

}
